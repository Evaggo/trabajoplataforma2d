﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform targetToFollow;
    
    private void Update () {
        this.transform.position = new Vector3(targetToFollow.transform.position.x,targetToFollow.transform.position.y+2, this.transform.position.z);
    }
}
