﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MenuManager : MonoBehaviour
{
    public GameObject audios;
    private int musiquita;
    public AudioSource musik;

    private void Awake()
    {
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            musik.mute = true;
        }
        else if (musiquita == 0)
        {
            musik.mute = false;
        }
    }

    public void SinglePlayer()
    {
        SceneManager.LoadScene("PlayerSelect");
    }

    public void PulsaOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }

    public void PulsaSoundON()
    {
        musik.mute = false;
       PlayerPrefs.SetInt("Musica", 0);
        /*
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            musik.mute = true;
        } else if (musiquita == 0)
        {
            musik.mute = false;
        }*/
    }

    public void PulsaSoundOFF()
    {
        musik.mute = true;
        PlayerPrefs.SetInt("Musica", 1);
        /*
                musiquita = PlayerPrefs.GetInt("Musica");
                if (musiquita == 1)
                {
                    musik.mute = true;
                }
                else if (musiquita == 0)
                {
                   musik.mute = false;
                }*/
    }

    public void PulsaGoBack()
    {
        SceneManager.LoadScene("Menu");
    }

}
