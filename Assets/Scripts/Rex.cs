﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Rex : MonoBehaviour
{
    private float shootTime = 0;
    public OndaWeapon weapon;
    private float timeCount;
    private int musiquita;

    public float maxS = 11f;
    public float fuerzaSalto = 5.0f;
    public GameObject chico;
    public GameObject chica;
    public Text name;
    public Text vidasTxt;

    private Vector2 checkPoint;
    private Vector2 checkPoint1;
    private string playerName;
    private int vidas;
    private Rigidbody2D rb2d = null;
    private Collider2D body;
    private int coleccionable = 0;
       
    private bool isGrounded = false;
    private bool jump;
    private bool attack;
    private bool isJumping;
    private bool isDead;

    private float move = 0f;
    private float scalaActual;
    private int CharacterSelect;

    [SerializeField] private GameObject graphics;
    [SerializeField] private Animator animator1;
    [SerializeField] private Animator animator2;

    public AudioSource attackAudio;
    public AudioSource coleccionableAudio;
    public AudioSource muerteAudio;
    public AudioSource backgroundMusic;

    // Use this for initialization
    void Awake()
    {
        checkPoint = new Vector2(this.transform.position.x, this.transform.position.y);
        vidas = 3;
        rb2d = GetComponent<Rigidbody2D>();
        body = GetComponent<Collider2D>();
        CharacterSelect = PlayerPrefs.GetInt("CharSelected");
        if (CharacterSelect == 0)
        {
            chico.SetActive(true);
            chica.SetActive(false);
        }
        else if (CharacterSelect == 1) {
            chico.SetActive(false);
            chica.SetActive(true);
        }
        playerName = PlayerPrefs.GetString("PlayerName");
        name.text = playerName;
        vidasTxt.text = "Vidas: " + vidas;

        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            attackAudio.mute = true;
            coleccionableAudio.mute = true;
            muerteAudio.mute = true;
            backgroundMusic.mute = true;
        }
        else if (musiquita == 0)
        {
            attackAudio.mute = false;
            coleccionableAudio.mute = false;
            muerteAudio.mute = false;
            backgroundMusic.mute = false;
        }
    }
    
    void FixedUpdate()
    {
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        animator1.SetFloat("Velocity", Math.Abs(rb2d.velocity.x));
        animator2.SetFloat("Velocity", Math.Abs(rb2d.velocity.x));
    }

    private void Update()
    {

        shootTime += Time.deltaTime;
        vidasTxt.text = "Vidas: " + vidas;

        if (isDead == false) {
            move = Input.GetAxis("Horizontal");
            jump = Input.GetKeyDown(KeyCode.Space);
            attack = Input.GetButton("Fire1");

            if (attack && isGrounded)
            {
                move = 0f;
                jump = false;
                attackAudio.Play();
                animator1.SetBool("Hit", attack);
                animator2.SetBool("Hit", attack);
                Shoot();
            }
            else
            {
                animator1.SetBool("Hit", false);
                animator2.SetBool("Hit", false);
            }

            if (jump && isGrounded)
            {
                jump = false;
                isJumping = true;
                rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
                animator1.SetBool("Jump", isJumping);
                animator2.SetBool("Jump", isJumping);
            }

            scalaActual = graphics.transform.localScale.x;

            if (move > 0 && scalaActual < 0)
            {
                graphics.transform.localScale = new Vector3(1, 1, 1);
            }
            else if (move < 0 && scalaActual > 0)
            {
                graphics.transform.localScale = new Vector3(-1, 1, 1);
            }
        }
    }

    public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            weapon.Shoot();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrounded = true;
        isJumping = false;
        animator1.SetBool("Jump", isJumping);
        animator2.SetBool("Jump", isJumping);

        if (collision.gameObject.tag == "Coleccionable")
        {
            coleccionableAudio.Play();
            coleccionable++;
        } 
        else if (collision.gameObject.tag == "NextLevel")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else if (collision.gameObject.tag == "Checkpoint")
        {
            checkPoint = new Vector2 (this.transform.position.x, this.transform.position.y);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Plataforma") {
            isGrounded = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Muerte") {
            StartCoroutine(MUERTE());
        }

        if (other.gameObject.tag == "PowerUp") {
            maxS *=4;
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "Trampolin") {
            isJumping = true; 
            jump = false;
            rb2d.AddForce(Vector2.up * fuerzaSalto*2, ForceMode2D.Impulse);
            animator1.SetTrigger("Jump");
            animator2.SetTrigger("Jump");
        }

        if (other.gameObject.tag == "Vida")
        {
            vidas++;
            Destroy(other.gameObject);
        }
    }

        IEnumerator MUERTE()
    {
        animator1.SetTrigger("Dead");
        animator2.SetTrigger("Dead");
        isDead = true;
        muerteAudio.Play();
        vidas--;
        yield return new WaitForSeconds(1.0f);

        if (vidas > 0)
        {
            transform.position = new Vector2(checkPoint.x, checkPoint.y);
            yield return new WaitForSeconds(0.1f);
            isDead = false;
            animator1.SetTrigger("Revive");
            animator2.SetTrigger("Revive");
        } else if (vidas == 0) {
            SceneManager.LoadScene("Game Over");
        }
    }
}
