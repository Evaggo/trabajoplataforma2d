﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OndaWeapon : OndaExpansiva
{
    public GameObject ondaBullet;
    public float cadencia;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(ondaBullet, this.transform.position, Quaternion.identity, null);
    }
}
