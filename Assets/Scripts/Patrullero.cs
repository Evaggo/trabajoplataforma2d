﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform [] posiciones;
    int posActual = 0;
    public int enemySpeed;

    private void Update() 
    {
        if (Vector2.Distance(posiciones[posActual].position, transform.position) < 0.1f)
        {
            posActual++;
            if (posActual >= posiciones.Length)
            {
                posActual = 0;
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, posiciones[posActual].position, Time.deltaTime*enemySpeed);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "bullet")
        {
            Destroy(this.gameObject);
        }
    }
}
