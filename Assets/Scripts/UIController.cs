﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public GameObject boy;
    public GameObject girl;
    int playerprefs;

    private void Awake()
    {
        playerprefs = PlayerPrefs.GetInt("CharSelected");
        if(playerprefs == 1)
        {
            boy.SetActive(false);
            girl.SetActive(true);
        } else if (playerprefs == 0)
        {
            boy.SetActive(true);
            girl.SetActive(false);
        }

    }
}
