﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    //public AudioSource buttonSound;
    [SerializeField] GameObject canvas;


    public void Continue()
    {
        //buttonSound.Play();
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void Quit()
    {
        //buttonSound.Play();
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Menu");
    }

    void Update()
    {
        if (!isPaused && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
        {
            ActivatePause();
        }
        else if (isPaused && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
        {
            Continue();
        }
    }

    void ActivatePause()
    {
        isPaused = true;

        canvas.SetActive(true);

        Time.timeScale = 0;
    }
}
