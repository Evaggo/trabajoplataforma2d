﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.UI;

public class SelectMenu : MonoBehaviour
{

    public GameObject boy;
    public GameObject girl;
    public Text name;

    private void Awake()
    {
        PlayerPrefs.SetString("PlayerName", "Rylan");
    }

    private void Start()
    {
        PlayerPrefs.SetInt("CharSelected", 1);
    }

    public void PulsaNext()
    {
        SceneManager.LoadScene("Prologue");
    }
    public void ReturnMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void AcceptName()
    {
        PlayerPrefs.SetString("PlayerName", name.text);
    }

    public void BoyChar()
    {
        boy.SetActive(true);
        girl.SetActive(false);
        PlayerPrefs.SetInt("CharSelected", 0);
    }

    public void GirlChar()
    {
        boy.SetActive(false);
        girl.SetActive(true);
        PlayerPrefs.SetInt("CharSelected", 1);
    }

}
