﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class PrologueManager : MonoBehaviour
{
    public GameObject[] textos;
    public GameObject[] audios;
    private int i;
    private int musiquita;

    public AudioSource backgroundMusic;

    private void Awake()
    {
        musiquita = PlayerPrefs.GetInt("Musica");
        if (musiquita == 1)
        {
            //audios[i].mute = true;
            backgroundMusic.mute = true;
        }
        else if (musiquita == 0)
        {
            //audios[i].mute = true;  
            backgroundMusic.mute = false;
        }

        i = 0;
        textos[i].SetActive(true);
        audios[i].SetActive(true);
    }
    public void Next()
    {
        
        if (musiquita == 1)
        {
            i++;
            textos[i - 1].SetActive(false);
            textos[i].SetActive(true);
            audios[i - 1].SetActive(false);
            audios[i].SetActive(false);
        }
        else if (musiquita == 0)
        {
            i++;
            textos[i - 1].SetActive(false);
            textos[i].SetActive(true);
            audios[i - 1].SetActive(false);
            audios[i].SetActive(true);
        }
        /*
        i++;
        textos[i - 1].SetActive(false);
        textos[i].SetActive(true);
        audios[i - 1].SetActive(false);
        audios[i].SetActive(true);
        */
    }

    public void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene("Plataforma");
        } else if (i == 9) {
            StartCoroutine(GoMenu());
        }
    }

    IEnumerator GoMenu() {
        yield return new WaitForSeconds(7f);
        SceneManager.LoadScene("Plataforma");
    }
}
