﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OndaExpansiva : MonoBehaviour
{
    public abstract float GetCadencia();

    public abstract void Shoot();
}
